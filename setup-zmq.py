from setuptools import setup

install_requires = [
    "pyzmq==18.0.1",
    "numpy==1.16.3"
]

setup( 
    name='pyzmq-messaging',
    version='2.8.4',
    description='Embedded microservices efficient messaging mechanism via ZeroMQ ... distributed processing && task coordination',
    long_description='Not given yet !',
    author='Miltos Vimplis',
    url='https://gitlab.com:mvimplis2013/messageQ-client',
    license='Apache license 2.0',
    packages=['messageqclient'],
    #package_data=[],
    entry_points={
        'console_scripts': [
            'zeromq-heartbeat-ping=messageqclient.zero.heartbeat.ping:main',
            'zeromq-heartbeat-pong=messageqclient.zero.heartbeat.pong:main'
        ]
    }, 
    install_requires=install_requires,
    #extras_require={},
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Programming Language :: Python :: 3.4',
        'Topic :: Storage :: Memory :: Key-Value :: Pub/Sub :: Listener :: Topic',
    ],
    zip_safe=True
)

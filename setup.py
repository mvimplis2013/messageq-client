from setuptools import setup

install_requires = [
    "redis==3.2.1",
]

setup( 
    name='redis-client',
    version='2.8.4',
    description='Redis client to support the Local-Agent short-term storage requirements',
    long_description='Not given yet !',
    author='Miltos Vimplis',
    url='https://gitlab.com:mvimplis2013/stealth-container',
    license='Apache license 2.0',
    packages=['messageqclient'],
    #package_data=[],
    entry_points={
        'console_scripts': [
            'messageqclient=messageqclient.__main__:main'
        ]
    }, 
    install_requires=install_requires,
    #extras_require={},
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Programming Language :: Python :: 3.4',
        'Topic :: Storage :: Memory :: Key-Value :: Pub/Sub :: Listener :: Topic',
    ],
    zip_safe=True
)

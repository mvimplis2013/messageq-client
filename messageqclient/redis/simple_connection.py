import redis 

from . import *

import os
host = os.environ["MESSAGE-QUEUE-HOST"]
port = os.environ["MESSAGE-QUEUE-PORT"]
db   = os.environ["MESSAGE-QUEUE-DB"]

def establish_single_connection():
    logger.debug("Trying to establish a network connection with Redis")
    return redis.StrictRedis(host=host, port=port, db=db)

def establish_pool_connection():
    pass

import redis

def delete_all_keys_current( redis_client ):
    return redis_client.flushdb()
import time
import numpy
import zmq
from zmq import devices

import logging 

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

def main():
    logger.debug("Received a Ping ... I will Respond with a Pong !")
    ctx = zmq.Context()

    #dev = devices.ThreadDevice(zmq.FORWARDER, zmq.REP, -1)
    socket = ctx.socket(zmq.REP)
    #dev.bind_in("tcp://*:10111")
    socket.bind("tcp://*:10111")
    #dev.setsockopt_in(zmq.IDENTITY, b'whoda')
    #dev.connect_in("tcp://zeromq-pong:10111")
    #dev.connect_out("tcp://zeromq-pong:10112")

    #dev.start()

    #time.sleep(1)

    #A = numpy.random.random((2**11, 2**12))

    while True:
        message = socket.recv()

        #tic = time.time()
        #numpy.dot(A, A.transpose())
        #logger.debug( "Blocked for %.3f secs", (time.time() - tic))

        logger.debug("Received Request: %s", message)
        
        time.sleep(1)

        socket.send_string("Hello WORLD from ... " )

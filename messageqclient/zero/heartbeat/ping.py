import sys
import time 

import numpy
import zmq

import logging 

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

def main():
    ctx = zmq.Context()

    req = ctx.socket(zmq.REQ)
    req.connect("tcp://zeromq-pong:10111")

    # wait for connects
    time.sleep(1)
    n = 0

    while True:
        time.sleep(numpy.random.random())

        for i in range(4):
            n += 1
            
            msg = "ping %i" % n

            tic = time.time()
            req.send_string(msg)
            resp = req.recv_string()

            logger.debug("%s: %.2f ms --> %s", msg, 1000*(time.time() - tic), resp)

            #assert msg == resp
